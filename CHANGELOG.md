## Version 2.1.1
- 发布2.1.1正式版本

## Version 2.1.1-rc.1

- 修改hasOwnProperty的使用问题
- 适配DevEco Studio: 4.1 Canary(4.1.3.322), SDK: API11 (4.1.0.36)

## Version 2.1.1-rc.0

- 修复不兼容API9问题

## Version 2.1.0

- 1.ArkTS语法整改
- 2.适配DevEco Studio: 4.0 (4.0.3.512), SDK: API10 (4.0.10）
- 3.接口使用方式变更：GlobalContext替代globalThis

## Version 2.0.1

- 更改包名的大小写

## Version 2.0.0

1.适配DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
2.包管理工具由npm切换成ohpm

## Version 1.1.0

- 名称由XmlGraphicsBatikETS修改为XmlGraphicsBatik；
- 旧的包@ohos/XmlGraphicsBatikETS已不再维护，请使用新包@ohos/XmlGraphicsBatik；

## Version 1.0.6

- 适配API9Stage模型；


## Version 1.0.5

- hvigor工程结构整改；
- index.ets 内部分文件导出方式修改；

## Version 1.0.3

- 支持SVG图像的显示，可显示静态及动态SVG图像；
- 支持快捷生成SVG图像文件；
- 支持操作SVG图像进行颜色、样式、内容的修改；
- 支持将SVG图像的xml文本解析为可操作对象。